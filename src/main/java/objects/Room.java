package objects;

import java.util.Timer;

public class Room {
    private int roomId;
    private PatientState patientState;
    private Timer timer;
    private int healthPoints;

    public Room(int roomId, PatientState patientState, int healthPoints) {
        this.roomId = roomId;
        this.patientState = patientState;
        this.healthPoints = healthPoints;
        this.timer  = new Timer(roomId + " room health.");
        this.timer.schedule(null,10,8);
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public PatientState getPatientState() {
        return patientState;
    }

    public void setPatientState(PatientState patientState) {
        this.patientState = patientState;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }
}
